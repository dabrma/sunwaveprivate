﻿using System;
using System.Linq;
using System.Security.Cryptography;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.EntityFrameworkCore;
using SunWaveModels.Models;

namespace SunWaveBackend.Context
{
    public class DatabaseEntities : DbContext
    {
        private string connectionString;
        public DatabaseEntities(string connectionString) : base()
        {
            this.connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityUserLogin>().HasKey(key => key.UserId);
            modelBuilder.Entity<IdentityUserRole>().HasKey(key => key.UserId);
               
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<CashBankRecord> CashBankRecords { get; set; }
        public DbSet<CashBankRegister> CashBankRegisters { get; set; }
        public DbSet<CashBankReport> CashBankReports { get; set; }
        public DbSet<Commodity> Commodities { get; set; }
        public DbSet<CommodityIn> CommoditiesIn { get; set; }
        public DbSet<CommodityOut> CommoditiesOut { get; set; }
        public DbSet<Counterparty> Counterparties { get; set; }
        public DbSet<CosmeticSiteResource> CosmeticSiteResources { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<CurrencyExchangeRate> CurrencyExchangeRates { get; set; }
        public DbSet<DeviceResource> DeviceResources { get; set; }
        public DbSet<DocumentDefinition> DocumentDefinitions { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeGroupPermission> EmployeeGroupPermissions { get; set; }
        public DbSet<HDSiteResource> HDSiteResources { get; set; }
        public DbSet<MassageSiteResource> MassageSiteResources { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<PriceDefinition> PriceDefinitions { get; set; }
        public DbSet<Quantity> Quantities { get; set; }
        public DbSet<QuantityState> QuantityStates { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<ScheduleElement> ScheduleElements { get; set; }
        public DbSet<Settlement> Settlements { get; set; }
        public DbSet<SiteResource> SiteResources { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SolariumResource> SolariumResources { get; set; }
        public DbSet<SpaSiteResource> SpaSiteResources { get; set; }
        public DbSet<TradeWarehouseHeader> TradeWarehouseHeaders { get; set; }
        public DbSet<TradeWarehouseItem> TradeWarehouseItems { get; set; }
        public DbSet<TradeWarehouseVAT> TradeWarehouseVATs { get; set; }
        public DbSet<UnitOfMeasure> UnitsOfMeasure { get; set; }
        public DbSet<VATRate> VATRates { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }
    }
}