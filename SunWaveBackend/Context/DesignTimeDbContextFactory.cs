﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace SunWaveBackend.Context
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DatabaseEntities>
    {
        public DatabaseEntities CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DatabaseEntities>();

            var connectionString = configuration.GetConnectionString("DefaultConnection");

            builder.UseMySql("Server=localhost;User Id=root;password=root;Database=test-aa");

            return new DatabaseEntities("Server=localhost;User Id=root;password=root;Database=test-aa");
        }
    }
}