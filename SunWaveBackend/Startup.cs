﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SunWaveBackend.Context;
using SunWaveBackend.Helpers;
using SunWaveModels.Models;

namespace SunWaveBackend
{
    public class Startup
    {
        private const string ConnectionStringSectionName = "ConnectionStrings";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private IServiceCollection services;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            this.services = services;
            this.services.AddIdentityServer().AddDeveloperSigningCredential();
            AutoMapperConfiguration.Configure();
            services.AddMvcCore().AddAuthorization().AddJsonFormatters();
            // this.services.AddDbContext<DatabaseEntities>();

            var builder = new ContainerBuilder();
            RegisterDatabases(builder);
            ServiceContainer.Container = builder.Build();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider svp)
        {
            HttpContextUtils.Configure(app.ApplicationServices
                .GetRequiredService<IHttpContextAccessor>());
            this.services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddIdentity<UserAccount, IdentityRole>().AddUserStore<DatabaseEntities>().AddDefaultTokenProviders();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = "http://foo.com",
                    ValidIssuer = "http://foo.com",
                    IssuerSigningKey =
                        new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes("this is my custom Secret key for authnetication"))
                };
            });


            app.UseMvc();
        }

        private void RegisterDatabases(ContainerBuilder builder)
        {
            var connectionStrings = new Dictionary<string, string>();
            Configuration.GetSection(ConnectionStringSectionName).Bind(connectionStrings);

            foreach (var connectionString in connectionStrings)
            {
                builder.RegisterType<DatabaseEntities>().Named<DatabaseEntities>(connectionString.Key).WithParameter("connectionString", connectionString.Value);
            }
        }
    }
}
