﻿using AutoMapper;
using SunWaveModels.DTO;
using SunWaveModels.Models;

namespace SunWaveBackend.Helpers
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Employee, EmployeeDTO>();
                cfg.CreateMap<Counterparty, CounterpartyDTO>();
            });
        }
    }
}