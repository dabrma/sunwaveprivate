﻿namespace SunWaveBackend.Helpers
{
    public static class HttpContextUtils
    {
        private static Microsoft.AspNetCore.Http.IHttpContextAccessor HttpContextAccessor;

        public static void Configure(Microsoft.AspNetCore.Http.IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }


        public static Microsoft.AspNetCore.Http.HttpContext Current
        {
            get { return HttpContextAccessor.HttpContext; }
        }
    }
}