﻿using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace SunWaveBackend.Helpers
{
    public class RequestReader
    {
        public static string ReadRequestBody(HttpRequest request)
        {
            using (StreamReader reader = new StreamReader(request.Body, Encoding.UTF8))
            {
                var result = reader.ReadToEnd();

                if (string.IsNullOrEmpty(result))
                {
                    throw new Exception("Request body was null or empty and couldn't be processed");
                }

                return result;
            }
        }
    }
}