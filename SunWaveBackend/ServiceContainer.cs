﻿using Autofac;

namespace SunWaveBackend
{
    public static class ServiceContainer
    {
        public static IContainer Container;

        public static T ResolveNamedDependency<T>(string serviveName)
        {
            return Container.ResolveNamed<T>(serviveName);
        }
    }
}