﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using SunWaveBackend.Context;
using SunWaveBackend.Helpers;

namespace SunWaveBackend.Controllers
{
    public class ResolvableContextBase<T> : ControllerBase
        where T  : DbContext
    {
        private const string VendorKeyToken = "vendor";
        public readonly T Context;

        public ResolvableContextBase()
        {
            this.Context = ServiceContainer.ResolveNamedDependency<T>(GetVendorNameFromRequestHeader(VendorKeyToken));
        }

        private string GetVendorNameFromRequestHeader(string vendorKey)
        {
            var databaseVendorNameParam = HttpContextUtils.Current.Request.Headers[vendorKey];
            return databaseVendorNameParam;
        }
    }
}