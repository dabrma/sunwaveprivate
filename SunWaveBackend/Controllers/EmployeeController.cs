﻿using System.Collections.Generic;
using AutoMapper;
using IdentityServer4.Extensions;
using Microsoft.AspNetCore.Mvc;
using SunWaveBackend.Context;
using SunWaveModels.DTO;
using SunWaveModels.Models;

namespace SunWaveBackend.Controllers
{
    [Route("api/[controller]/[action]")]
    public class EmployeeController : ResolvableContextBase<DatabaseEntities>
    {
        #region Fields & Constructors
        [HttpPost]
        public IActionResult CreateEmployee([FromBody] EmployeeDTO employeeDto)
        {
            using (Context)
            {
                var employee = Mapper.Map<Employee>(employeeDto);
                Context.Add(employee);
                return Ok(Context.SaveChanges());
            }
        }

        [HttpPost]
        public IActionResult EditEmployee(int? id, [FromBody] EmployeeDTO employeeDto)
        {
            var originalEmployee = Context.Employees.Find(id);
            if (originalEmployee == null)
            {
                return NotFound();
            }

            Context.Entry(originalEmployee).CurrentValues.SetValues(employeeDto);
            return Ok();
        }

        [HttpGet]
        public ActionResult<EmployeeDTO> GetEmployee(int id)
        {
            var employee = Context.Employees.Find(id);

            if (employee == null)
            {
                return NotFound();
            }

            return Mapper.Map<EmployeeDTO>(employee);
        }

        [HttpGet]
        public ActionResult<List<EmployeeDTO>> GetCounterparties(int[] employeeIds)
        {
            var temp = new List<EmployeeDTO>();

            foreach (int id in employeeIds)
            {
                var employee = Context.Employees.Find(id);

                if (employee == null)
                {
                    return NotFound();
                }

                temp.Add(Mapper.Map<EmployeeDTO>(employee));
            }

            if (temp.IsNullOrEmpty())
            {
                return NotFound();
            }

            return temp;
        }
        #endregion
    }
}