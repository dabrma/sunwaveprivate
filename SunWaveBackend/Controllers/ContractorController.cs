﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using SunWaveBackend.Context;
using SunWaveModels.DTO;
using SunWaveModels.Models;
using AutoMapper;
using IdentityServer4.Extensions;

namespace SunWaveBackend.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ContractorController : ResolvableContextBase<DatabaseEntities>
    {
        [HttpPost]
        public IActionResult CreateContractor([FromBody] CounterpartyDTO counterpartyDto)
        {
            using (Context)
            {
                var counterparty = Mapper.Map<Counterparty>(counterpartyDto);
                Context.Add(counterparty);
                return Ok(Context.SaveChanges());
            }
        }

        [HttpPost]
        public IActionResult EditContractor(int? id, [FromBody] CounterpartyDTO counterpartyDto)
        {
            var originalCounterparty = Context.Counterparties.Find(id);
            if (originalCounterparty == null)
            {
                return NotFound();
            }

            Context.Entry(originalCounterparty).CurrentValues.SetValues(counterpartyDto);
            return Ok();
        }

        [HttpGet]
        public ActionResult<CounterpartyDTO> GetCounterparty(int id)
        {
            var counterparty = Context.Counterparties.Find(id);

            if (counterparty == null)
            {
                return NotFound();
            }

            return Mapper.Map<CounterpartyDTO>(counterparty);
        }

        [HttpGet]
        public ActionResult<List<CounterpartyDTO>> GetCounterparties(int[] counterpartyIds)
        {
            var temp = new List<CounterpartyDTO>();

            foreach (int id in counterpartyIds)
            {
                var counterparty = Context.Counterparties.Find(id);

                if (counterparty == null)
                {
                    return NotFound();
                }

                temp.Add(Mapper.Map<CounterpartyDTO>(counterparty));
            }

            if (temp.IsNullOrEmpty())
            {
                return NotFound();
            }

            return temp;
        }
    }
}