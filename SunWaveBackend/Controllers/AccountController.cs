﻿using AutoMapper;
using SunWaveModels.DTO;

namespace SunWaveBackend.Controllers
{
    #region Usings
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;
    using Context;
    using Helpers;
    using SunWaveModels.Models;
    #endregion

    [Route("api/[controller]/[action]")]
    public class AccountController : ResolvableContextBase<DatabaseEntities>
    {
        [HttpPost]
        public IActionResult CreateUserAccount([FromBody] UserAccount account)
        {
            using (this.Context)
            {
                if (this.Context.UserAccounts.Any(acc => acc.UserName == account.UserName))
                {
                    return NotFound();
                }

                this.Context.Add(account);
                this.Context.SaveChanges();
            }

            return Accepted();
        }

        [HttpPost]
        public IActionResult UpdateUserAccount(int id, [FromBody] UserAccount userAccount)
        {
            using (this.Context)
            {
                var account = Context.UserAccounts.Find(id);

                this.Context.Entry(account).CurrentValues.SetValues(userAccount);
                return Ok();
            }
        }
    }
}