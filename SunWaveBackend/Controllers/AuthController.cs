﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SunWaveBackend.Context;
using SunWaveBackend.Helpers;
using SunWaveModels;
using SunWaveModels.DTO;
using SunWaveModels.Models;

namespace SunWaveBackend.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : ResolvableContextBase<DatabaseEntities>
    {
        public AuthController()
        {
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] LoginDTO account)
        {
            var userAcc = Context.UserAccounts.FirstOrDefault(user => user.UserName == account.Username);
            if (CheckPass(userAcc, account.Password))
            {

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, account.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                var token = new JwtSecurityToken(
                    issuer: "http://foo.com",
                    audience: "http://foo.com",
                    expires: DateTime.UtcNow.AddHours(1),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this is my custom Secret key for authnetication")),
                        SecurityAlgorithms.HmacSha256),
                    claims: claims
                );

                return Accepted(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                });
            }

            return Ok();
        }

        bool CheckPass(UserAccount acc, string pwdString)
        {
            var hashedPwd = PasswordSecurity.Hash(pwdString, acc.Salt);
            return acc.StoredPassword.Equals(hashedPwd);
        }
    }
}