﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SunWaveBackend.Migrations
{
    public partial class ef : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CashBankRecords",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DocumentsDefinitionsId = table.Column<int>(nullable: false),
                    CurrenciesId = table.Column<int>(nullable: false),
                    Ammount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    ContractorId = table.Column<int>(nullable: false),
                    No = table.Column<int>(nullable: false),
                    NoSchema = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    FullNo = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    DateOfIssue = table.Column<DateTime>(nullable: false),
                    Descriptions = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashBankRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CashBankRegisters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Symbol = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    Name = table.Column<string>(type: "NVARCHAR(40)", maxLength: 40, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Period = table.Column<int>(nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    DocumentsDefinitionsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashBankRegisters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CashBankReports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    No = table.Column<int>(nullable: false),
                    NoSchema = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    FullNo = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    DateFrom = table.Column<DateTime>(nullable: false),
                    DateTo = table.Column<DateTime>(nullable: false),
                    Closed = table.Column<bool>(nullable: false),
                    ValueOpen = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    ValueClose = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    CurrencyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashBankReports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommoditiesIn",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ResourceId = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    ReservedQuantity = table.Column<double>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommoditiesIn", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CommoditiesOut",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ResourceId = table.Column<int>(nullable: false),
                    TradeWarehouseHeaderId = table.Column<int>(nullable: false),
                    TradeWarehouseItemId = table.Column<int>(nullable: false),
                    DateOperation = table.Column<DateTime>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    ReservedQuantity = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommoditiesOut", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractorApprovals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ContractorId = table.Column<int>(nullable: false),
                    ApprovalId = table.Column<int>(nullable: false),
                    Accepted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractorApprovals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Counterparties",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    LastName = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Name1 = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Name2 = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Name3 = table.Column<string>(type: "NVARCHAR(250)", maxLength: 250, nullable: true),
                    PostalCode = table.Column<string>(type: "NVARCHAR(10)", maxLength: 10, nullable: true),
                    City = table.Column<string>(type: "NVARCHAR(40)", maxLength: 40, nullable: true),
                    Street = table.Column<string>(type: "NVARCHAR(40)", maxLength: 40, nullable: true),
                    VatId = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    VatCountry = table.Column<string>(type: "NVARCHAR(2)", maxLength: 2, nullable: true),
                    Country = table.Column<string>(type: "NVARCHAR(40)", maxLength: 40, nullable: true),
                    Mail = table.Column<string>(type: "NVARCHAR(120)", maxLength: 120, nullable: true),
                    Phone1M = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Phone2 = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    LoyaltyCardNumber = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    PriceLevel = table.Column<int>(nullable: false),
                    Discount = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contractors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CosmeticSiteResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ResourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CosmeticSiteResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currencies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Symbol = table.Column<string>(type: "NVARCHAR(3)", maxLength: 3, nullable: true),
                    System = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CurrencyExchangeRates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CurrenciesId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Rate = table.Column<decimal>(type: "decimal(10,4)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyExchangeRates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeviceResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    ResourceId = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentDefinitions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<int>(nullable: false),
                    Symbol = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    Name = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    SchemaNo = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Direction = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentDefinitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeApprovals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EmployeeId = table.Column<int>(nullable: false),
                    ApprovalId = table.Column<int>(nullable: false),
                    Accepted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeApprovals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeGroupPermissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EmployeesGroupsId = table.Column<int>(nullable: false),
                    PermissionsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeGroupPermissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeGroups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeGroups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    LastName = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Operator = table.Column<bool>(nullable: false),
                    Mail = table.Column<string>(type: "NVARCHAR(120)", maxLength: 120, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Phone1M = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Phone2 = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeSkills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EmployeeId = table.Column<int>(nullable: false),
                    SkillId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeSkills", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FirmApprovals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApprovalId = table.Column<int>(nullable: false),
                    Accepted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FirmApprovals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HDSiteResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ResourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HDSiteResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MassageSiteResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ResourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MassageSiteResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    PaymentDayQuantity = table.Column<int>(nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    BankName = table.Column<string>(type: "NVARCHAR(250)", maxLength: 250, nullable: true),
                    BankAccount = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    BankAccountIBAN = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(250)", maxLength: 250, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Object = table.Column<string>(type: "NVARCHAR(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PriceDefinitions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(10)", maxLength: 10, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceDefinitions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Quantities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CommodityId = table.Column<int>(nullable: false),
                    WarehouseId = table.Column<int>(nullable: false),
                    QuantityStart = table.Column<double>(nullable: false),
                    QuantityRemained = table.Column<double>(nullable: false),
                    Reservation = table.Column<double>(nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    CurrencyExchangeRateId = table.Column<int>(nullable: false),
                    NetValueSystemCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueSystemCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    NetValueCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    TradeWarehouseHeaderId = table.Column<int>(nullable: false),
                    TradeWarehousePositionsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quantities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuantityStates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CommodityId = table.Column<int>(nullable: false),
                    WarehouseId = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Reservation = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuantityStates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ScheduleElements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SchedulerId = table.Column<int>(nullable: false),
                    ResourceDeviceId = table.Column<int>(nullable: false),
                    Descriptions = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    DateFrom = table.Column<DateTime>(nullable: false),
                    DateTo = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleElements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Schedules",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ContractorId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    DateFrom = table.Column<DateTime>(nullable: false),
                    DateTo = table.Column<DateTime>(nullable: false),
                    ResourceId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Settlements",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CashBankRecordsID = table.Column<int>(nullable: false),
                    TraWearHeaderId = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settlements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SiteResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Location = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Description = table.Column<string>(type: "NVARCHAR(250)", maxLength: 250, nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(250)", maxLength: 250, nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SolariumResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ResourceId = table.Column<int>(nullable: false),
                    SerialNo = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    InternalNo = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    CabinNo = table.Column<string>(type: "NVARCHAR(10)", maxLength: 10, nullable: true),
                    ControllerNo = table.Column<string>(type: "NVARCHAR(15)", maxLength: 15, nullable: true),
                    PortNo = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    Address = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    DescriptionClientProducer = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    DescriptionProvider = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    DescriptionOwn = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    LampsBody = table.Column<bool>(nullable: false),
                    LampsBodyPower = table.Column<int>(nullable: false),
                    LampsBodyQuantity = table.Column<int>(nullable: false),
                    LampsBodyStart = table.Column<int>(nullable: false),
                    LampsBodyConsumption = table.Column<int>(nullable: false),
                    LampsBodyMaxConsumption = table.Column<int>(nullable: false),
                    LampsBodyMaxConsumptionServis = table.Column<int>(nullable: false),
                    LampsBodyTypeId = table.Column<int>(nullable: false),
                    LampsBodyDescription = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    LampsFace = table.Column<bool>(nullable: false),
                    LampsFacePower = table.Column<int>(nullable: false),
                    LampsFaceQuantity = table.Column<int>(nullable: false),
                    LampsFaceStart = table.Column<int>(nullable: false),
                    LampsFaceConsumption = table.Column<int>(nullable: false),
                    LampsFaceMaxConsumption = table.Column<int>(nullable: false),
                    LampsFaceMaxConsumptionServis = table.Column<int>(nullable: false),
                    LampsFaceTypeId = table.Column<int>(nullable: false),
                    LampsFaceDescription = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    LampsShoulders = table.Column<bool>(nullable: false),
                    LampsShouldersPower = table.Column<int>(nullable: false),
                    LampsShouldersQuantity = table.Column<int>(nullable: false),
                    LampsShouldersStart = table.Column<int>(nullable: false),
                    LampsShouldersConsumption = table.Column<int>(nullable: false),
                    LampsShouldersMaxConsumption = table.Column<int>(nullable: false),
                    LampsShouldersMaxConsumptionServis = table.Column<int>(nullable: false),
                    LampsShouldersTypeId = table.Column<int>(nullable: false),
                    LampsShouldersDescription = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    LampsShouldersNeck = table.Column<bool>(nullable: false),
                    LampsShouldersNeckPower = table.Column<int>(nullable: false),
                    LampsShouldersNeckQuantity = table.Column<int>(nullable: false),
                    LampsShouldersNeckStart = table.Column<int>(nullable: false),
                    LampsShouldersNeckConsumption = table.Column<int>(nullable: false),
                    LampsShouldersNeckMaxConsumption = table.Column<int>(nullable: false),
                    LampsShouldersNeckMaxConsumptionServis = table.Column<int>(nullable: false),
                    LampsShouldersNeckTypeId = table.Column<int>(nullable: false),
                    LampsShouldersNeckDescription = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    EffFanPower = table.Column<int>(nullable: false),
                    EffStart = table.Column<int>(nullable: false),
                    EffConsumption = table.Column<int>(nullable: false),
                    AquaBrief = table.Column<bool>(nullable: false),
                    Aroma = table.Column<bool>(nullable: false),
                    AirConditioning = table.Column<bool>(nullable: false),
                    Audio = table.Column<bool>(nullable: false),
                    Bluetooth = table.Column<bool>(nullable: false),
                    MP3 = table.Column<bool>(nullable: false),
                    ServiceInfoAgree = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SolariumResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SpaSiteResources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ResourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SpaSiteResources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TradeWarehouseHeaders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DocumentsDefinitionsId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    No = table.Column<int>(nullable: false),
                    NoSchema = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    DateOfIssue = table.Column<DateTime>(nullable: false),
                    DateOfSellBuy = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    DateReceived = table.Column<DateTime>(nullable: false),
                    FullNo = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    CurrenciesId = table.Column<int>(nullable: false),
                    CurrenciesExchRateId = table.Column<int>(nullable: false),
                    NetValueSystemCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueSystemCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    NetValueCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    CostCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    CostSystemCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    Status = table.Column<int>(nullable: false),
                    DateReservation = table.Column<DateTime>(nullable: false),
                    DateExecution = table.Column<DateTime>(nullable: false),
                    ProviderNo = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Descriptions = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    SendToAccount = table.Column<bool>(nullable: false),
                    SendToAccountDate = table.Column<DateTime>(nullable: false),
                    WarehouseSourceId = table.Column<int>(nullable: false),
                    WarehouseDestinationId = table.Column<int>(nullable: false),
                    PaymentWay = table.Column<int>(nullable: false),
                    PaymentDayQuantity = table.Column<int>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    ContractorId = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    LastName = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    ContractorStatus = table.Column<int>(nullable: false),
                    Name1 = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Name2 = table.Column<string>(type: "NVARCHAR(50)", maxLength: 50, nullable: true),
                    Name3 = table.Column<string>(type: "NVARCHAR(250)", maxLength: 250, nullable: true),
                    PostalCode = table.Column<string>(type: "NVARCHAR(10)", maxLength: 10, nullable: true),
                    City = table.Column<string>(type: "NVARCHAR(40)", maxLength: 40, nullable: true),
                    Street = table.Column<string>(type: "NVARCHAR(40)", maxLength: 40, nullable: true),
                    VatId = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    VatCountry = table.Column<string>(type: "NVARCHAR(2)", maxLength: 2, nullable: true),
                    Country = table.Column<string>(type: "NVARCHAR(40)", maxLength: 40, nullable: true),
                    Corrected = table.Column<bool>(nullable: false),
                    CorrectionLastId = table.Column<int>(nullable: false),
                    CorrectedFirstId = table.Column<int>(nullable: false),
                    CorrectedId = table.Column<int>(nullable: false),
                    CorrectionId = table.Column<int>(nullable: false),
                    Correction = table.Column<bool>(nullable: false),
                    Canceled = table.Column<bool>(nullable: false),
                    Settled = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    PercentDiscount = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    FromNet = table.Column<bool>(nullable: false),
                    NetValueSystemCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueSystemCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    NetValueCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeWarehouseHeaders", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TradeWarehouseItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TraWearHeaderId = table.Column<int>(nullable: false),
                    OrdinalNo = table.Column<int>(nullable: false),
                    OrdinalNo2 = table.Column<int>(nullable: false),
                    Aggregated = table.Column<bool>(nullable: false),
                    AggregatedOrdinalNo = table.Column<int>(nullable: false),
                    Corrected = table.Column<bool>(nullable: false),
                    CorrectionLastId = table.Column<int>(nullable: false),
                    CorrectedFirstId = table.Column<int>(nullable: false),
                    CorrectedId = table.Column<int>(nullable: false),
                    CorrectionId = table.Column<int>(nullable: false),
                    Correction = table.Column<bool>(nullable: false),
                    CmmodietesId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(type: "NVARCHAR(100)", maxLength: 100, nullable: true),
                    UnitOfMeasureId = table.Column<int>(nullable: false),
                    UnitOfMeasureSymbol = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    UnitOfMeasureWarehouseConverter = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    QuantityWarehouse = table.Column<double>(nullable: false),
                    VatId = table.Column<int>(nullable: false),
                    VatSymbol = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    VatValue = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    NetPriceCurrencyWithDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    NetPriceSystemCurrencyWithDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    GrossPriceCurrencyWithDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    GrossPriceSystemCurrencyWithDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    NetValueCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    NetValueSystemCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueSystemCurrencyWithDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    CostCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    CostSystemCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    PriceCommodietesId = table.Column<int>(nullable: false),
                    NetPriceCurrencyNoDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    NetPriceSystemCurrencyNoDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    GrossPriceCurrencyNoDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    GrossPriceSystemCurrencyNoDiscount = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    NetValueCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    NetValueSystemCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrossValueSystemCurrencyNoDiscount = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    PercentDiscount = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    PriceHand = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeWarehouseItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TradeWarehouseVATs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TraWearHeaderId = table.Column<int>(nullable: false),
                    VatId = table.Column<int>(nullable: false),
                    VatSymbol = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    VatValue = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    NetValueCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    NetValueSystemCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrosValueCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    GrosValueSystemCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    ValueVatCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    ValueVatSystemCurrency = table.Column<decimal>(type: "decimal(15,2)", nullable: false),
                    CurrenciesId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TradeWarehouseVATs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnitsOfMeasure",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Symbol = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitsOfMeasure", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VATRates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Symbol = table.Column<string>(type: "NVARCHAR(5)", maxLength: 5, nullable: true),
                    Value = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VATRates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Warehouses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Warehouses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserAccounts",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEndDateUtc = table.Column<DateTime>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Id = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    EmployeeId = table.Column<int>(nullable: false),
                    StoredPassword = table.Column<string>(nullable: false),
                    Salt = table.Column<byte[]>(type: "VARBINARY(16)", maxLength: 16, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccounts", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_UserAccounts_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Commodities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Name = table.Column<string>(type: "NVARCHAR(100)", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    EAN = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    ProviderCode = table.Column<string>(type: "NVARCHAR(20)", maxLength: 20, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ProviderDescription = table.Column<string>(type: "NVARCHAR(1024)", maxLength: 1024, nullable: true),
                    UoMWarehouseId = table.Column<int>(nullable: false),
                    UoMWarehouseConverterSell = table.Column<int>(nullable: false),
                    UoMWarehouseConverterBuy = table.Column<int>(nullable: false),
                    VATSellId = table.Column<int>(nullable: false),
                    VATBuyId = table.Column<int>(nullable: false),
                    UoMSellId = table.Column<int>(nullable: false),
                    UoMBuyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commodities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Commodities_UnitsOfMeasure_UoMBuyId",
                        column: x => x.UoMBuyId,
                        principalTable: "UnitsOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Commodities_UnitsOfMeasure_UoMSellId",
                        column: x => x.UoMSellId,
                        principalTable: "UnitsOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Commodities_VATRates_VATBuyId",
                        column: x => x.VATBuyId,
                        principalTable: "VATRates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Commodities_VATRates_VATSellId",
                        column: x => x.VATSellId,
                        principalTable: "VATRates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "IdentityUserClaim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: true),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserAccountUserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserClaim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IdentityUserClaim_UserAccounts_UserAccountUserId",
                        column: x => x.UserAccountUserId,
                        principalTable: "UserAccounts",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IdentityUserLogin",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: true),
                    ProviderKey = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false),
                    UserAccountUserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserLogin", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_IdentityUserLogin_UserAccounts_UserAccountUserId",
                        column: x => x.UserAccountUserId,
                        principalTable: "UserAccounts",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IdentityUserRole",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: true),
                    UserAccountUserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IdentityUserRole", x => x.UserId);
                    table.ForeignKey(
                        name: "FK_IdentityUserRole_UserAccounts_UserAccountUserId",
                        column: x => x.UserAccountUserId,
                        principalTable: "UserAccounts",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Commodities_UoMBuyId",
                table: "Commodities",
                column: "UoMBuyId");

            migrationBuilder.CreateIndex(
                name: "IX_Commodities_UoMSellId",
                table: "Commodities",
                column: "UoMSellId");

            migrationBuilder.CreateIndex(
                name: "IX_Commodities_VATBuyId",
                table: "Commodities",
                column: "VATBuyId");

            migrationBuilder.CreateIndex(
                name: "IX_Commodities_VATSellId",
                table: "Commodities",
                column: "VATSellId");

            migrationBuilder.CreateIndex(
                name: "IX_IdentityUserClaim_UserAccountUserId",
                table: "IdentityUserClaim",
                column: "UserAccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_IdentityUserLogin_UserAccountUserId",
                table: "IdentityUserLogin",
                column: "UserAccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_IdentityUserRole_UserAccountUserId",
                table: "IdentityUserRole",
                column: "UserAccountUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccounts_EmployeeId",
                table: "UserAccounts",
                column: "EmployeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CashBankRecords");

            migrationBuilder.DropTable(
                name: "CashBankRegisters");

            migrationBuilder.DropTable(
                name: "CashBankReports");

            migrationBuilder.DropTable(
                name: "Commodities");

            migrationBuilder.DropTable(
                name: "CommoditiesIn");

            migrationBuilder.DropTable(
                name: "CommoditiesOut");

            migrationBuilder.DropTable(
                name: "ContractorApprovals");

            migrationBuilder.DropTable(
                name: "Counterparties");

            migrationBuilder.DropTable(
                name: "CosmeticSiteResources");

            migrationBuilder.DropTable(
                name: "Currencies");

            migrationBuilder.DropTable(
                name: "CurrencyExchangeRates");

            migrationBuilder.DropTable(
                name: "DeviceResources");

            migrationBuilder.DropTable(
                name: "DocumentDefinitions");

            migrationBuilder.DropTable(
                name: "EmployeeApprovals");

            migrationBuilder.DropTable(
                name: "EmployeeGroupPermissions");

            migrationBuilder.DropTable(
                name: "EmployeeGroups");

            migrationBuilder.DropTable(
                name: "EmployeeSkills");

            migrationBuilder.DropTable(
                name: "FirmApprovals");

            migrationBuilder.DropTable(
                name: "HDSiteResources");

            migrationBuilder.DropTable(
                name: "IdentityUserClaim");

            migrationBuilder.DropTable(
                name: "IdentityUserLogin");

            migrationBuilder.DropTable(
                name: "IdentityUserRole");

            migrationBuilder.DropTable(
                name: "MassageSiteResources");

            migrationBuilder.DropTable(
                name: "PaymentMethods");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "PriceDefinitions");

            migrationBuilder.DropTable(
                name: "Quantities");

            migrationBuilder.DropTable(
                name: "QuantityStates");

            migrationBuilder.DropTable(
                name: "ScheduleElements");

            migrationBuilder.DropTable(
                name: "Schedules");

            migrationBuilder.DropTable(
                name: "Settlements");

            migrationBuilder.DropTable(
                name: "SiteResources");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropTable(
                name: "SolariumResources");

            migrationBuilder.DropTable(
                name: "SpaSiteResources");

            migrationBuilder.DropTable(
                name: "TradeWarehouseHeaders");

            migrationBuilder.DropTable(
                name: "TradeWarehouseItems");

            migrationBuilder.DropTable(
                name: "TradeWarehouseVATs");

            migrationBuilder.DropTable(
                name: "Warehouses");

            migrationBuilder.DropTable(
                name: "UnitsOfMeasure");

            migrationBuilder.DropTable(
                name: "VATRates");

            migrationBuilder.DropTable(
                name: "UserAccounts");

            migrationBuilder.DropTable(
                name: "Employees");
        }
    }
}
