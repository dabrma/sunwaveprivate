using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class SolariumResource
    {
        public SolariumResource()
        {
            this.SerialNo = string.Empty;
            this.InternalNo = string.Empty;
            this.CabinNo = string.Empty;
            this.ControllerNo = string.Empty;
            this.PortNo = string.Empty;
            this.Address = string.Empty;
            this.DescriptionClientProducer = string.Empty;
            this.DescriptionProvider = string.Empty;
            this.DescriptionOwn = string.Empty;
            this.LampsBody = false;
            this.LampsBodyPower = 0;
            this.LampsBodyQuantity = 0;
            this.LampsBodyStart = 0;
            this.LampsBodyConsumption = 0;
            this.LampsBodyMaxConsumption = 0;
            this.LampsBodyMaxConsumptionServis = 0;
            this.LampsBodyDescription = string.Empty;
            this.LampsFace = false;
            this.LampsFacePower = 0;
            this.LampsFaceQuantity = 0;
            this.LampsFaceStart = 0;
            this.LampsFaceConsumption = 0;
            this.LampsFaceMaxConsumption = 0;
            this.LampsFaceMaxConsumptionServis = 0;
            this.LampsFaceDescription = string.Empty;
            this.LampsShoulders = false;
            this.LampsShouldersPower = 0;
            this.LampsShouldersQuantity = 0;
            this.LampsShouldersStart = 0;
            this.LampsShouldersConsumption = 0;
            this.LampsShouldersMaxConsumption = 0;
            this.LampsShouldersMaxConsumptionServis = 0;
            this.LampsShouldersDescription = string.Empty;
            this.LampsShouldersNeck = false;
            this.LampsShouldersNeckPower = 0;
            this.LampsShouldersNeckQuantity = 0;
            this.LampsShouldersNeckStart = 0;
            this.LampsShouldersNeckConsumption = 0;
            this.LampsShouldersNeckMaxConsumption = 0;
            this.LampsShouldersNeckMaxConsumptionServis = 0;
            this.LampsShouldersNeckDescription = string.Empty;
            this.EffFanPower = 0;
            this.EffStart = 0;
            this.EffConsumption = 0;
            this.AquaBrief = false;
            this.Aroma = false;
            this.AirConditioning = false;
            this.Audio = false;
            this.Bluetooth = false;
            this.MP3 = false;
            this.ServiceInfoAgree = false;
        }

        public int Id { get; set; }

        [Required]
        public int ResourceId { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string SerialNo { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string InternalNo { get; set; }

        [Column(TypeName = "NVARCHAR(10)")]
        [StringLength(10)]
        public string CabinNo { get; set; }

        [Column(TypeName = "NVARCHAR(15)")]
        [StringLength(15)]
        public string ControllerNo { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string PortNo { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Address { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string DescriptionClientProducer { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string DescriptionProvider { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string DescriptionOwn { get; set; }

        public bool LampsBody { get; set; }

        public int LampsBodyPower { get; set; }

        public int LampsBodyQuantity { get; set; }

        public int LampsBodyStart { get; set; }

        public int LampsBodyConsumption { get; set; }

        public int LampsBodyMaxConsumption { get; set; }

        public int LampsBodyMaxConsumptionServis { get; set; }

        public int LampsBodyTypeId { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string LampsBodyDescription { get; set; }

        public bool LampsFace { get; set; }

        public int LampsFacePower { get; set; }

        public int LampsFaceQuantity { get; set; }

        public int LampsFaceStart { get; set; }

        public int LampsFaceConsumption { get; set; }

        public int LampsFaceMaxConsumption { get; set; }

        public int LampsFaceMaxConsumptionServis { get; set; }

        public int LampsFaceTypeId { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string LampsFaceDescription { get; set; }

        public bool LampsShoulders { get; set; }

        public int LampsShouldersPower { get; set; }

        public int LampsShouldersQuantity { get; set; }

        public int LampsShouldersStart { get; set; }

        public int LampsShouldersConsumption { get; set; }

        public int LampsShouldersMaxConsumption { get; set; }

        public int LampsShouldersMaxConsumptionServis { get; set; }

        public int LampsShouldersTypeId { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string LampsShouldersDescription { get; set; }

        public bool LampsShouldersNeck { get; set; }

        public int LampsShouldersNeckPower { get; set; }

        public int LampsShouldersNeckQuantity { get; set; }

        public int LampsShouldersNeckStart { get; set; }

        public int LampsShouldersNeckConsumption { get; set; }

        public int LampsShouldersNeckMaxConsumption { get; set; }

        public int LampsShouldersNeckMaxConsumptionServis { get; set; }

        public int LampsShouldersNeckTypeId { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string LampsShouldersNeckDescription { get; set; }

        public int EffFanPower { get; set; }

        public int EffStart { get; set; }

        public int EffConsumption { get; set; }

        public bool AquaBrief { get; set; }

        public bool Aroma { get; set; }

        public bool AirConditioning { get; set; }

        public bool Audio { get; set; }

        public bool Bluetooth { get; set; }

        public bool MP3 { get; set; }

        public bool ServiceInfoAgree { get; set; }

    }
}
