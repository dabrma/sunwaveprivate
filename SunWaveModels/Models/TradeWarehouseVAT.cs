using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class TradeWarehouseVAT
    {
        public TradeWarehouseVAT()
        {
            this.VatSymbol = string.Empty;
            this.VatValue = 0;
            this.NetValueCurrency = 0;
            this.NetValueSystemCurrency = 0;
            this.GrosValueCurrency = 0;
            this.GrosValueSystemCurrency = 0;
            this.ValueVatCurrency = 0;
            this.ValueVatSystemCurrency = 0;
        }

        public int Id { get; set; }

        [Required]
        public int TraWearHeaderId { get; set; }

        [Required]
        public int VatId { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string VatSymbol { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal VatValue { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueSystemCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrosValueCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrosValueSystemCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal ValueVatCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal ValueVatSystemCurrency { get; set; }

        [Required]
        public int CurrenciesId { get; set; }

    }
}
