using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class PaymentMethod
    {
        public PaymentMethod()
        {
            this.Name = string.Empty;
            this.Type = 1;
            this.PaymentDayQuantity = 0;
            this.BankName = string.Empty;
            this.BankAccount = string.Empty;
            this.BankAccountIBAN = string.Empty;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Name { get; set; }

        public int Type { get; set; }

        public int PaymentDayQuantity { get; set; }

        [Required]
        public int CurrencyId { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string BankName { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string BankAccount { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string BankAccountIBAN { get; set; }

    }
}
