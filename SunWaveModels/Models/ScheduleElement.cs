using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class ScheduleElement
    {
        public ScheduleElement()
        {
            this.Descriptions = string.Empty;
        }

        public int Id { get; set; }

        [Required]
        public int SchedulerId { get; set; }

        [Required]
        public int ResourceDeviceId { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string Descriptions { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

    }
}
