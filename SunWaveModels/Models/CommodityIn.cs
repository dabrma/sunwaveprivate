using System;
using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class CommodityIn
    {
        public CommodityIn()
        {
            this.Quantity = 0;
            this.ReservedQuantity = 0;
            this.Date = DateTime.Now;
        }

        public int Id { get; set; }

        [Required]
        public int  ResourceId { get; set; }

        public double  Quantity{ get; set; }

        public double  ReservedQuantity { get; set; }

        public DateTime Date { get; set; }

    }
}
