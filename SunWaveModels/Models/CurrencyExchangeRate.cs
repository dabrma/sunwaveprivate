using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class CurrencyExchangeRate
    {
        public CurrencyExchangeRate()
        {
            this.Rate = 0.0000M;
        }

        public int Id { get; set; }

        [Required]
        public int CurrenciesId { get; set; }

        public DateTime Date { get; set; }

        [Column(TypeName = "decimal(10,4)")]
        public decimal Rate { get; set; }

    }
}
