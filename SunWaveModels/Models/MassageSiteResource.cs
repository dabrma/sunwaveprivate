using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class MassageSiteResource 
    {
        public MassageSiteResource()
        {
        }

        public int Id { get; set; }

        [Required]
        public int ResourceId { get; set; }

    }
}
