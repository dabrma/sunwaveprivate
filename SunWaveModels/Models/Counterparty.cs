using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Counterparty
    {
        public Counterparty()
        {
            this.IsActive = false;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Name1 = string.Empty;
            this.Name2 = string.Empty;
            this.Name3 = string.Empty;
            this.PostalCode = string.Empty;
            this.City = string.Empty;
            this.Street = string.Empty;
            this.VatId = null;
            this.VatCountry = null;
            this.Country = string.Empty;
            this.Mail = string.Empty;
            this.Phone1M = string.Empty;
            this.Phone2 = string.Empty;
            this.LoyaltyCardNumber = string.Empty;
            this.PriceLevel = 1;
            this.Discount = 0;
            this.Default = false;
        }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string LastName { get; set; }

        public int Status { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string Name1 { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string Name2 { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Name3 { get; set; }

        [Column(TypeName = "NVARCHAR(10)")]
        [StringLength(10)]
        public string PostalCode { get; set; }

        [Column(TypeName = "NVARCHAR(40)")]
        [StringLength(40)]
        public string City { get; set; }

        [Column(TypeName = "NVARCHAR(40)")]
        [StringLength(40)]
        public string Street { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string VatId { get; set; }

        [Column(TypeName = "NVARCHAR(2)")]
        [StringLength(2)]
        public string VatCountry { get; set; }

        [Column(TypeName = "NVARCHAR(40)")]
        [StringLength(40)]
        public string Country { get; set; }

        [Column(TypeName = "NVARCHAR(120)")]
        [StringLength(120)]
        public string Mail { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Phone1M { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Phone2 { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string LoyaltyCardNumber { get; set; }

        public int PriceLevel { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal Discount { get; set; }

        public DateTime DateOfBirth { get; set; }

        public bool Default { get; set; }

        public virtual ICollection<Approval> ContractorApprovals { get; set; }
    }
}
