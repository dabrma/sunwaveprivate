using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class EmployeeGroup
    {
        public EmployeeGroup()
        {
            this.Name = string.Empty;
            this.IsActive = true;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Name { get; set; }

        public bool IsActive { get; set; }
    }
}
