using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class TradeWarehouseHeader
    {
        public TradeWarehouseHeader()
        {
            this.No = 0;
            this.NoSchema = string.Empty;
            this.DateOfIssue = DateTime.Now;
            this.DateOfSellBuy = DateTime.Now;
            this.Date = DateTime.Now;
            this.FullNo = string.Empty;
            this.NetValueSystemCurrencyNoDiscount = 0;
            this.GrossValueSystemCurrencyNoDiscount = 0;
            this.NetValueCurrencyNoDiscount = 0;
            this.GrossValueCurrencyNoDiscount = 0;
            this.CostCurrency = 0;
            this.CostSystemCurrency = 0;
            this.Status = 1;
            this.ProviderNo = string.Empty;
            this.Descriptions = string.Empty;
            this.SendToAccount = false;
            this.SendToAccountDate = DateTime.Now;
            this.PaymentDayQuantity = 0;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.ContractorStatus = 1;
            this.Name1 = string.Empty;
            this.Name2 = string.Empty;
            this.Name3 = string.Empty;
            this.PostalCode = string.Empty;
            this.City = string.Empty;
            this.Street = string.Empty;
            this.VatId = string.Empty;
            this.VatCountry = string.Empty;
            this.Country = string.Empty;
            this.Corrected = false;
            this.Correction = false;
            this.Canceled = false;
            this.Settled = 0;
            this.PercentDiscount = 0;
            this.FromNet = false;
            this.NetValueSystemCurrencyWithDiscount = 0;
            this.GrossValueSystemCurrencyWithDiscount = 0;
            this.NetValueCurrencyWithDiscount = 0;
            this.GrossValueCurrencyWithDiscount = 0;
        }

        public int Id { get; set; }

        [Required]
        public int DocumentsDefinitionsId { get; set; }

        [Required]
        public int Type { get; set; }

        public int No { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string NoSchema { get; set; }

        public DateTime DateOfIssue { get; set; }

        public DateTime DateOfSellBuy { get; set; }

        public DateTime Date { get; set; }

        public DateTime DateReceived { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string FullNo { get; set; }

        [Required]
        public int CurrenciesId { get; set; }

        public int CurrenciesExchRateId { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueSystemCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueSystemCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal CostCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal CostSystemCurrency { get; set; }

        public int Status { get; set; }

        public DateTime DateReservation { get; set; }

        public DateTime DateExecution { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string ProviderNo { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string Descriptions { get; set; }

        public bool SendToAccount { get; set; }

        public DateTime SendToAccountDate { get; set; }

        public int WarehouseSourceId { get; set; }

        public int WarehouseDestinationId { get; set; }

        [Required]
        public int PaymentWay { get; set; }

        public int PaymentDayQuantity { get; set; }

        public DateTime PaymentDate { get; set; }

        [Required]
        public int ContractorId { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string LastName { get; set; }

        public int ContractorStatus { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string Name1 { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string Name2 { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Name3 { get; set; }

        [Column(TypeName = "NVARCHAR(10)")]
        [StringLength(10)]
        public string PostalCode { get; set; }

        [Column(TypeName = "NVARCHAR(40)")]
        [StringLength(40)]
        public string City { get; set; }

        [Column(TypeName = "NVARCHAR(40)")]
        [StringLength(40)]
        public string Street { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string VatId { get; set; }

        [Column(TypeName = "NVARCHAR(2)")]
        [StringLength(2)]
        public string VatCountry { get; set; }

        [Column(TypeName = "NVARCHAR(40)")]
        [StringLength(40)]
        public string Country { get; set; }

        public bool Corrected { get; set; }

        public int CorrectionLastId { get; set; }

        public int CorrectedFirstId { get; set; }

        public int CorrectedId { get; set; }

        public int CorrectionId { get; set; }

        public bool Correction { get; set; }

        public bool Canceled { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal Settled { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal PercentDiscount { get; set; }

        public bool FromNet { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueSystemCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueSystemCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueCurrencyWithDiscount { get; set; }

    }
}
