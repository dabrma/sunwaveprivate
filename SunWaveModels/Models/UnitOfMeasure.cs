using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class UnitOfMeasure
    {
        public UnitOfMeasure()
        {
            this.Symbol = string.Empty;
            this.IsActive = true;
            this.Default = false;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string Symbol { get; set; }

        public bool IsActive { get; set; }

        public bool Default { get; set; }

    }
}
