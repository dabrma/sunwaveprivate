using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class PriceDefinition
    {
        public PriceDefinition()
        {
            this.Name = string.Empty;
            this.IsActive = true;
            this.Default = false;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(10)")]
        [StringLength(10)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public bool Default { get; set; }

    }
}
