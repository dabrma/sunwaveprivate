using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Approval
    {
        public Approval()
        {
            this.Name = string.Empty;
            this.IsActive = true;
            this.Type = 1;
            this.Version = 1;
            this.Path = string.Empty;
            this.Accepted = false;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int Type { get; set; }

        public int Version { get; set; }

        [Required]
        public int OriginId { get; set; }

        [Required]
        public int PreviousId { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Path { get; set; }

        public bool Accepted { get; set; }
    }
}
