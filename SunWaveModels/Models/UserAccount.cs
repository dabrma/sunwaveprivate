﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNetCore.Identity;

namespace SunWaveModels.Models
{
    public class UserAccount : IdentityUser
    {

        public UserAccount() : base()
        {
            new RNGCryptoServiceProvider().GetBytes(Salt = new byte[16]);
        }

        [Key]
        public int UserId { get; set; }
        public int EmployeeId { get; set; }

//        [Required]
//        public string Username { get; set; }

        [NotMapped]
        public string Password {
            set
            {
                if (value == null)
                {
                    throw new Exception();
                }

                StoredPassword = PasswordSecurity.Hash(value, Salt);
            } 
        }

        [Required]
        public string StoredPassword { get; set; }

        [Required]
        [MaxLength(16), Column(TypeName = "VARBINARY(16)")]
        public byte[] Salt { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
