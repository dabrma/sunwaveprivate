using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class CashBankReport
    {
        public CashBankReport()
        {
            this.No = 0;
            this.NoSchema = string.Empty;
            this.FullNo = string.Empty;
            this.ValueOpen = 0;
            this.ValueClose = 0;
        }

        public int Id { get; set; }

        public int No { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string NoSchema { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string FullNo { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public bool Closed { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal ValueOpen { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal ValueClose { get; set; }

        [Required]
        public int CurrencyId { get; set; }

    }
}
