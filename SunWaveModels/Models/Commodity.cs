using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Commodity
    {
        public Commodity()
        {
            this.Code = string.Empty;
            this.Name = string.Empty;
            this.Description = string.Empty;
            this.EAN = null;
            this.Type = 1;
            this.ProviderCode = string.Empty;
            this.IsActive = true;
            this.ProviderDescription = string.Empty;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Code { get; set; }

        [Column(TypeName = "NVARCHAR(100)")]
        [StringLength(100)]
        public string Name { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string Description { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string EAN { get; set; }

        public int Type { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string ProviderCode { get; set; }

        public bool IsActive { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string ProviderDescription { get; set; }

        [Required]
        public int UoMWarehouseId { get; set; }

        [Required]
        public int UoMWarehouseConverterSell { get; set; }

        [Required]
        public int UoMWarehouseConverterBuy { get; set; }

        [Required]
        public int VATSellId { get; set; }

        [Required]
        public int VATBuyId { get; set; }

        [Required]
        public int UoMSellId { get; set; }

        [Required]
        public int UoMBuyId { get; set; }

        [Required]
        public virtual UnitOfMeasure UoMWarehouse { get; set; }

        [Required]
        public virtual VATRate VatSell { get; set; }

        [Required]
        public virtual VATRate VatBuy { get; set; }

        [Required]
        public virtual UnitOfMeasure UoMSell { get; set; }

        [Required]
        public virtual UnitOfMeasure UoMBuy { get; set; }
    }
}
