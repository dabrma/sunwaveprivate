using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class CashBankRecord
    {
        public CashBankRecord()
        {
            this.Ammount = 0;
            this.No = 0;
            this.NoSchema = string.Empty;
            this.FullNo = string.Empty;
            this.Descriptions = string.Empty;
        }

        public int Id { get; set; }

        [Required]
        public int DocumentsDefinitionsId { get; set; }

        [Required]
        public int CurrenciesId { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal Ammount { get; set; }

        [Required]
        public int ContractorId { get; set; }

        public int No { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string NoSchema { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string FullNo { get; set; }

        public DateTime DateOfIssue { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string Descriptions { get; set; }

    }
}
