using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class DeviceResource
    {
        public DeviceResource()
        {
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        public int ResourceId { get; set; }

        public bool IsActive { get; set; }

    }
}
