using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Skill
    {
        public Skill()
        {
            this.Name = string.Empty;
            this.IsActive = true;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

    }
}
