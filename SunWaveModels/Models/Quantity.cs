using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Quantity
    {
        public Quantity()
        {
            this.QuantityStart = 0;
            this.QuantityRemained = 0;
            this.Reservation = 0;
            this.NetValueSystemCurrency = 0;
            this.GrossValueSystemCurrency = 0;
            this.NetValueCurrency = 0;
            this.GrossValueCurrency = 0;
        }

        public int Id { get; set; }

        [Required]
        public int CommodityId { get; set; }

        [Required]
        public int WarehouseId { get; set; }

        public double QuantityStart { get; set; }

        public double QuantityRemained { get; set; }

        public double Reservation { get; set; }

        [Required]
        public int CurrencyId { get; set; }

        [Required]
        public int CurrencyExchangeRateId { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueSystemCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueSystemCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueCurrency { get; set; }

        [Required]
        public int TradeWarehouseHeaderId { get; set; }

        [Required]
        public int TradeWarehousePositionsId { get; set; }

    }
}
