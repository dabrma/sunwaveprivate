using System;
using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class QuantityState
    {
        public QuantityState()
        {
            this.Quantity = 0;
            this.Reservation = 0;
        }

        public int Id { get; set; }

        [Required]
        public int CommodityId { get; set; }

        public int WarehouseId { get; set; }

        public double Quantity { get; set; }

        public DateTime Date { get; set; }

        public double Reservation { get; set; }

    }
}
