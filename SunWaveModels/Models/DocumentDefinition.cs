using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class DocumentDefinition
    {
        public DocumentDefinition()
        {
            this.Symbol = string.Empty;
            this.Name = string.Empty;
            this.SchemaNo = string.Empty;
            this.Direction = 0;
            this.IsActive = true;
            this.Default = false;
        }

        public int Id { get; set; }

        [Required]
        public int Type { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string Symbol { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Name { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string SchemaNo { get; set; }

        public int Direction { get; set; }

        public bool IsActive { get; set; }

        public bool Default { get; set; }

    }
}
