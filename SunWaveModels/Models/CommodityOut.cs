using System;
using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class CommodityOut
    {
        public CommodityOut()
        {
            this.Quantity = 0;
            this.ReservedQuantity = 0;
        }

        public int Id { get; set; }

        [Required]
        public int ResourceId { get; set; }

        [Required]
        public int TradeWarehouseHeaderId { get; set; }

        [Required]
        public int TradeWarehouseItemId { get; set; }

        public DateTime DateOperation { get; set; }

        public double Quantity { get; set; }

        public double ReservedQuantity { get; set; }

    }
}
