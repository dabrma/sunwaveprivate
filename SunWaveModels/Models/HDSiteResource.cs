using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class HDSiteResource 
    {
        public HDSiteResource ()
        {
        }

        public int Id { get; set; }

        [Required]
        public int ResourceId { get; set; }

    }
}
