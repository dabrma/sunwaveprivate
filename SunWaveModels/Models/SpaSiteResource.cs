using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class SpaSiteResource
    {
        public SpaSiteResource()
        {
        }

        public int Id { get; set; }

        [Required]
        public int ResourceId { get; set; }

    }
}
