using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class VATRate
    {
        public VATRate()
        {
            this.Symbol = "23%";
            this.Value = 23;
            this.IsActive = true;
            this.Default = false;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string Symbol { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal Value { get; set; }

        public bool IsActive { get; set; }

        public bool Default { get; set; }

    }
}
