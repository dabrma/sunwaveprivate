using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class EmployeeGroupPermission
    {
        public EmployeeGroupPermission()
        {
        }

        public int Id { get; set; }

        [Required]
        public int EmployeesGroupsId { get; set; }

        [Required]
        public int PermissionsId { get; set; }

    }
}
