using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Schedule
    {
        public Schedule()
        {
            this.Description = string.Empty;
            this.Status = 1;
        }

        public int Id { get; set; }

        public int ContractorId { get; set; }

        public int EmployeeId { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public int ResourceId { get; set; }

        [Column(TypeName = "NVARCHAR(1024)")]
        [StringLength(1024)]
        public string Description { get; set; }

        public int Status { get; set; }

    }
}
