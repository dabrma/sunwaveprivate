using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class TradeWarehouseItem
    {
        public TradeWarehouseItem()
        {
            this.Aggregated = false;
            this.Corrected = false;
            this.Correction = false;
            this.Name = string.Empty;
            this.UnitOfMeasureSymbol = string.Empty;
            this.UnitOfMeasureWarehouseConverter = 1;
            this.Quantity = 0;
            this.QuantityWarehouse = 0;
            this.VatSymbol = string.Empty;
            this.VatValue = 0;
            this.NetPriceCurrencyWithDiscount = 0;
            this.NetPriceSystemCurrencyWithDiscount = 0;
            this.GrossPriceCurrencyWithDiscount = 0;
            this.GrossPriceSystemCurrencyWithDiscount = 0;
            this.NetValueCurrencyWithDiscount = 0;
            this.NetValueSystemCurrencyWithDiscount = 0;
            this.GrossValueCurrencyWithDiscount = 0;
            this.GrossValueSystemCurrencyWithDiscount = 0;
            this.CostCurrency = 0;
            this.CostSystemCurrency = 0;
            this.NetPriceCurrencyNoDiscount = 0;
            this.NetPriceSystemCurrencyNoDiscount = 0;
            this.GrossPriceCurrencyNoDiscount = 0;
            this.GrossPriceSystemCurrencyNoDiscount = 0;
            this.NetValueCurrencyNoDiscount = 0;
            this.NetValueSystemCurrencyNoDiscount = 0;
            this.GrossValueCurrencyNoDiscount = 0;
            this.GrossValueSystemCurrencyNoDiscount = 0;
            this.PercentDiscount = 0;
            this.PriceHand = false;
        }

        public int Id { get; set; }

        [Required]
        public int TraWearHeaderId { get; set; }

        public int OrdinalNo { get; set; }

        public int OrdinalNo2 { get; set; }

        public bool Aggregated { get; set; }

        public int AggregatedOrdinalNo { get; set; }

        public bool Corrected { get; set; }

        public int CorrectionLastId { get; set; }

        public int CorrectedFirstId { get; set; }

        public int CorrectedId { get; set; }

        public int CorrectionId { get; set; }

        public bool Correction { get; set; }

        [Required]
        public int CmmodietesId { get; set; }

        [Column(TypeName = "NVARCHAR(100)")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public int UnitOfMeasureId { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string UnitOfMeasureSymbol { get; set; }

        public int UnitOfMeasureWarehouseConverter { get; set; }

        public double Quantity { get; set; }

        public double QuantityWarehouse { get; set; }

        [Required]
        public int VatId { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string VatSymbol { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal VatValue { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal NetPriceCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal NetPriceSystemCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal GrossPriceCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal GrossPriceSystemCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueSystemCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueSystemCurrencyWithDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal CostCurrency { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal CostSystemCurrency { get; set; }

        [Required]
        public int PriceCommodietesId { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal NetPriceCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal NetPriceSystemCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal GrossPriceCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal GrossPriceSystemCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal NetValueSystemCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal GrossValueSystemCurrencyNoDiscount { get; set; }

        [Column(TypeName = "decimal(5,2)")]
        public decimal PercentDiscount { get; set; }

        public bool PriceHand { get; set; }

    }
}
