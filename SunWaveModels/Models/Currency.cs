using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Currency
    {
        public Currency()
        {
            this.Symbol = string.Empty;
            this.System = false;
            this.IsActive = true;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(3)")]
        [StringLength(3)]
        public string Symbol { get; set; }

        public bool System { get; set; }

        public bool IsActive { get; set; }

    }
}
