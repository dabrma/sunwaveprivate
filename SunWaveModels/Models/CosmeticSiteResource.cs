using System.ComponentModel.DataAnnotations;

namespace SunWaveModels.Models
{
    public class CosmeticSiteResource
    {
        public CosmeticSiteResource()
        {
        }

        public int Id { get; set; }

        [Required]
        public int ResourceId { get; set; }

    }
}
