using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Settlement
    {
        public Settlement()
        {
            this.Value = 0;
        }

        public int Id { get; set; }

        [Required]
        public int CashBankRecordsID { get; set; }

        [Required]
        public int TraWearHeaderId { get; set; }

        [Column(TypeName = "decimal(15,2)")]
        public decimal Value { get; set; }

        public DateTime Date { get; set; }

    }
}
