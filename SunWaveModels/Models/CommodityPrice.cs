using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class CommodityPrice
    {
        public CommodityPrice()
        {
            this.NetPrice = 0;
            this.GrossPrice = 0;
        }

        public int Id { get; set; }

        [Required]
        public int PriceDefinitionId { get; set; }

        [Required]
        public int CommodityId { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal NetPrice { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal GrossPrice { get; set; }

    }
}
