using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;

namespace SunWaveModels.Models
{
    public class Employee
    {
        public Employee()
        {
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Mail = string.Empty;
            this.IsActive = true;
            this.Phone1M = string.Empty;
            this.Phone2 = string.Empty;
            this.Type = 4;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Column(TypeName = "NVARCHAR(50)")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        public bool Operator { get; set; }

        [Column(TypeName = "NVARCHAR(120)")]
        [StringLength(120)]
        public string Mail { get; set; }

        public bool IsActive { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Phone1M { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Phone2 { get; set; }

        public int Type { get; set; }

        public virtual ICollection<EmployeeGroup> EmployeeGroups { get; set; }
        public virtual ICollection<Approval> EmployeeApprovals { get; set; }
        public virtual ICollection<Skill> EmployeeSkills { get; set; }
    }
}
