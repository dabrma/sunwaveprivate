using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class CashBankRegister
    {
        public CashBankRegister()
        {
            this.Symbol = string.Empty;
            this.Name = string.Empty;
            this.IsActive = true;
            this.Period = 1;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(5)")]
        [StringLength(5)]
        public string Symbol { get; set; }

        [Column(TypeName = "NVARCHAR(40)")]
        [StringLength(40)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int Period { get; set; }

        [Required]
        public int CurrencyId { get; set; }

        [Required]
        public int DocumentsDefinitionsId { get; set; }

    }
}
