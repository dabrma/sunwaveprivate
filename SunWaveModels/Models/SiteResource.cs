using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class SiteResource 
    {
        public SiteResource ()
        {
            this.Name = string.Empty;
            this.Location = string.Empty;
            this.Description = string.Empty;
            this.IsActive = true;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Name { get; set; }

        [Column(TypeName = "NVARCHAR(20)")]
        [StringLength(20)]
        public string Location { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Description { get; set; }

        public bool IsActive { get; set; }

    }
}
