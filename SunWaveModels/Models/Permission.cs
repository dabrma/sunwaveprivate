using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunWaveModels.Models
{
    public class Permission
    {
        public Permission()
        {
            this.Name = string.Empty;
            this.Type = 1;
            this.Object = string.Empty;
        }

        public int Id { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Name { get; set; }

        public int Type { get; set; }

        [Column(TypeName = "NVARCHAR(250)")]
        [StringLength(250)]
        public string Object { get; set; }

    }
}
