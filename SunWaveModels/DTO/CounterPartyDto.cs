﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunWaveModels.Models;

namespace SunWaveModels.DTO
{
    public class CounterpartyDTO
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Status { get; set; }

        public string Name1 { get; set; }

        public string Name2 { get; set; }

        public string Name3 { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string VatId { get; set; }

        public string VatCountry { get; set; }

        public string Country { get; set; }

        public string Mail { get; set; }

        public string Phone1M { get; set; }

        public string Phone2 { get; set; }

        public string LoyaltyCardNumber { get; set; }

        public int PriceLevel { get; set; }

        public decimal Discount { get; set; }

        public DateTime DateOfBirth { get; set; }

        public bool Default { get; set; }

        public List<Approval> ContractorAprrovals { get; set; }
    }
}