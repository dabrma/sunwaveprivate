﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SunWaveModels.Models;

namespace SunWaveModels.DTO
{
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Operator { get; set; }
        public string Mail { get; set; }
        public bool IsActive { get; set; }
        public string Phone1M { get; set; }
        public string Phone2 { get; set; }
        public int Type { get; set; }
        public virtual List<EmployeeGroup> EmployeeGroups { get; set; }
        public virtual List<Approval> EmployeeApprovals { get; set; }
        public virtual List<Skill> EmployeeSkills { get; set; }
    }
}