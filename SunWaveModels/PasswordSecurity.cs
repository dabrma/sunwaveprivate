﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SunWaveModels
{
    public class PasswordSecurity
    {
        public static string Hash(string value, byte[] salt)
        {
            return Hash(Encoding.UTF8.GetBytes(value), salt);
        }

        private static string Hash(byte[] value, byte[] salt)
        {
            byte[] saltedValue = value.Concat(salt).ToArray();
            var hash = new SHA256Managed().ComputeHash(saltedValue);
            StringBuilder sb = new StringBuilder();
            foreach(var byteValue in hash)
            {
                sb.Append(string.Format("{0:X}", byteValue));
            }
            return sb.ToString();
        }
    }
}